/*
** list_movements.c for bistromathique in /home/boguta_m/Projets/Bistro/convsrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Nov  6 17:22:46 2013 maxime boguta
** Last update Sat Nov  9 16:17:04 2013 maxime boguta
*/

#include <string.h>
#include "define.h"
#include "prototypes.h"

t_token		*go_to_start(t_token *list)
{
  while (list->prev != NULL)
    list = list->prev;
  return (list);
}

t_token		*go_to_end(t_token *list)
{
  while (list != NULL)
    list = list->next;
  return (list);
}

t_token		*go_to_last_bracket(t_token *list)
{
  while (list->type != OP_PAR)
    list = list->prev;
  list = list->prev;
  return (list);
}
