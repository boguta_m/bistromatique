/*
** to_NPI.c for eval_expr in /home/boguta_m/rendu/Piscine-C-eval_expr
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Sun Oct 27 19:57:15 2013 maxime boguta
** Last update Sat Nov  9 21:16:25 2013 maxime boguta
*/

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "define.h"
#include "prototypes.h"
#include "my.h"

t_token		*add_to_list(t_token *npi, t_token *list)
{
  t_token	*new;

  if (npi == NULL)
    {
      npi = malloc(sizeof(t_token));
      npi->val = my_strdup(list->val);
      npi->type = list->type;
      npi->sign[0] = list->sign[0];
      npi->prev = NULL;
      return (npi);
    }
  else
    {
      new = malloc(sizeof(t_token));
      npi->next = new;
      new->prev = npi;
      new->val = my_strdup(list->val);;
      new->type = list->type;
      new->next = NULL;
      return (new);
    }
}

t_token		*depile_all(t_token *pile, t_token *npi, char mode)
{
  if (mode == 0)
    {
      while (pile->type != OP_PAR)
	{
	  npi = add_to_list(npi, pile);
	  pile = pile->prev;
	}
      pile = pile->prev;
      return (npi);
    }
  if (mode == 1)
    {
      go_to_end(pile);
      while (pile != NULL)
	{
	  npi = add_to_list(npi, pile);
	  pile = pile->prev;
	}
      return (npi);
    }
} 

t_token		*set_npi(t_token *list, t_token *npi, t_token *pile)
{
  while (list != NULL)
    {
      if (list->type == OPERAND)
	npi = add_to_list(npi, list);
      else if (list->type == OPERATOR || list->type == PRI_OPERATOR)
	{
	  if (SWITCH_OUT)
	    {
	      npi = add_to_list(npi, pile);
	      pile = pile->prev;
	      pile = add_to_list(pile, list);
	    }
	  else
	    pile = add_to_list(pile, list);
	}
      else if (list->type == CL_PAR)
	{
	  npi = depile_all(pile, npi, 0);
	  pile = go_to_last_bracket(pile);
	}
      else
	pile = add_to_list(pile, list);
      list = list->next;
    }
  npi = depile_all(pile, npi, 1);
  return (npi);
}

t_token		*to_npi(t_token *list, t_args *vbox)
{
  t_token	*pile;
  t_token	*npi;
  
  pile = NULL;
  npi = NULL;
  npi = set_npi(list, npi, pile);
  while (npi->prev != NULL)
    npi = npi->prev;
  return (npi);
}
