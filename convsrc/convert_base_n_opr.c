/*
** convert_base.c for bistromathique in /home/boguta_m/Projets/Bistro/parsesrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Oct 31 13:57:03 2013 maxime boguta
** Last update Sun Nov 10 23:21:15 2013 maxime boguta
*/

#include <string.h>
#include "define.h"
#include "prototypes.h"
#include "bistromathique.h"

int		convert_op(t_token *list, t_args *vbox)
{
  while (list != NULL)
    {
      if (list->val[0] == vbox->op[0])
	list->val[0] = '(';
      else if (list->val[0] == vbox->op[1])
	list->val[0] = ')';
      else if (list->val[0] == vbox->op[2])
	list->val[0] = '+';
      else if (list->val[0] == vbox->op[3])
	list->val[0] = '-';
      else if (list->val[0] == vbox->op[4])
	list->val[0] = '*';
      else if (list->val[0] == vbox->op[5])
	list->val[0] = '/';
      else if (list->val[0] == vbox->op[6])
	list->val[0] = '%';
      else if (!IS_CHAR_OPD(list->val[0]))
	{
	  return (-1);
	}
      list = list->next;
    }
  return (0);
}

t_token		*convert_base_n_op(t_token *list, t_args *vbox)
{
  t_token	*save;

  save = list;
  if (convert_op(list, vbox) == -1)
    return (NULL);
  return (list);
}
