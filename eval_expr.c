/*
** eval_expr.c for bistromathique in /home/boguta_m/Projets/Bistro/parsesrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Oct 29 18:17:33 2013 maxime boguta
** Last update Sun Nov 10 23:38:36 2013 maxime boguta
*/

#include <string.h>
#include <stdlib.h>
#include "define.h"
#include "prototypes.h"
#include "bistromathique.h"

void		free_list(t_token *list)
{
  go_to_start(list);
  while (list != NULL)
    {
      if (list->prev != NULL)
	free(list->prev);
      list = list->next;
    }
}

char		*clear_spaces(char *str)
{
  int		i;
  char		*out;
  int		count_str;

  i = 0;
  count_str = 0;
  while (str[i] != 0)
    {
      if (str[i] == ' ')
	count_str = count_str - 1;
      i = i + 1;
      count_str = count_str + 1;
    }
  i = 0;
  out = malloc(sizeof(char) * count_str);
  while (*str != 0)
    {
      if (*str != ' ')
	{
	  out[i] = *str;
	  i = i + 1;
	}
      str = str + 1;
    }
  return (out);
}

char		*eval_expr(char *base,char *ops,char *expr,unsigned int size)
{
  t_token	*begin;
  char		*clear;
  t_args	 *vbox;

  vbox = store_arguments(vbox, base, ops);
  clear = clear_spaces(which_sign(expr, vbox));
  clear[my_strlen(clear)] = 0;
  begin = attrib_to_token(clear, vbox);
  if (begin == NULL)
    return (SYNTAXE_ERROR_MSG);
  begin = get_signs(begin, vbox);
  if (begin == NULL)
    return (SYNTAXE_ERROR_MSG);
  begin = to_npi(begin, vbox);
  return (start_calc(begin, vbox));
}
