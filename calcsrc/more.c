/*
** more.c for bistromathique in /home/boguta_m/Projets/Bistro/calcsrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Sun Nov 10 17:57:49 2013 maxime boguta
** Last update Sun Nov 10 19:42:07 2013 maxime boguta
*/

char		*replace_str(char *to_r, char *sub, int pos, int nb)
{
  int		i;

  i = 0;
  while (nb != 0)
    {
      to_r[pos + i] = sub[i];
      i++;
      nb--;
    }
  to_r = clean_zero(to_r + pos);
  return (to_r);
}      
      
