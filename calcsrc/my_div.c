/*
** my_div.c for bistromathique in /home/boguta_m/Projets/Bistro/calcsrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Nov  6 21:53:07 2013 maxime boguta
** Last update Sun Nov 10 20:12:54 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"
#include "define.h"
#include "defcalc.h"
#include "prototypes.h"

char		*get_tmp(char *str, int p, int p2)
{
  char		*ret;
  int		i;

  i = 0;
  ret = malloc((p2 - p) + 1);
  ret[(p2 - p) + 1] = 0;
  while (i < (p2 - p))
    {
      ret[i] = str[p + i];
      i++;
    }
  return (ret);
}
int             is_taller_4char(char *n1, char *n2)
{
  int           n1len;
  int           n2len;
  int           i;

  n1len = my_strlen(n1);
  n2len = my_strlen(n2);
  i = 0;
  if (n2len > n1len)
    return (1);
  else if (n2len < n1len)
    return (0);
  else if (n1len == n2len)
    while (i != n1len)
      {
        if (n2[i] > n1[i])
          return (1);
        else if (n2[i] == n1[i])
          i++;
	else
	  return (0);
      }
  return (0);
}

char		*do_it_div(char *n1, char *n2, char *res, t_args *vbox)
{
  int		push;
  int		take;
  char		*tmp;

  take = my_strlen(n2);
  push = my_strlen(n2);
  while (my_strlen(n1) > my_strlen(n2))
    {
      tmp = get_tmp(n1, 0, take);
      res[PUSHOUT] = '0';
      while (is_taller_4char(tmp, n2) == 0)
	{
	  take = my_strlen(n2);
	  tmp = my_sub(tmp, n2, vbox);
	  res[PUSHOUT] = res[PUSHOUT] + 1;
	}
      if (take == my_strlen(n2) && push != my_strlen(n2))
	n1 = replace_str(n1, tmp, take - my_strlen(n2) + 1, my_strlen(tmp));
      else
	n1 = replace_str(n1, tmp, take - my_strlen(n2), my_strlen(tmp));
      push++;
      if (res[my_strlen(res) - 1] == '0')
	take++;
    }
  if (*tmp == '0')
    while (*(n2 + 1) != 0)
      {
	res[PUSHOUT] = '0';
	push++;
	n2 = n2 + 1;
      }
  res[PUSHOUT] = 0;
  clean_zero(res);
  return (res);
}

char		*my_div(char *n1, char *n2, t_args *vbox)
{
  char		*res;
  int		t;

  if ((is_taller_4char(n1, n2) == 1))
    return ("0");
  res = malloc(my_strlen(n1) + 1);
  return (do_it_div(n1, n2, res, vbox));
}
