/*
** my_add.c for bistromathique in /home/boguta_m/Projets/Bistro/calcsrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Nov  6 21:52:38 2013 maxime boguta
** Last update Sun Nov 10 18:17:23 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"
#include "define.h"

char		*clean_zero(char *res)    
{
  int		i;

  i = 0;
  while (res[i] == '0')
    i = i + 1;
  if (i != my_strlen(res))
    return (res + i);
  else
    return (res + i - 1);
}

char		*adjust_res(char *res)
{
  int		t;
  int	        len;
  int		i;
  int		push;

  i = 0;
  push = 0;
  len = my_strlen(res);
  t = 0;
    while (i != 999)
      {
	while (t != len)
	  {
	    if (res[t] > '9')
	      {
		if (res[t - 1] == 0)
		  {
		    push = 1;
		    res[t - 1] = '1';
		    res[t] = res[t] - 10;
		  }
		else
		  {
		    res[t - 1]++;
		    res[t] = res[t] - 10;
		  }
	      }
	    t++;
	  }
	t = 0;
	i++;
      }
    return (res - push);
}    

char		*my_taller(char *n1, char *n2)
{
  if (my_strlen(n1) > my_strlen(n2))
    return (n1);
  else if (my_strlen(n1) < my_strlen(n2))
    return (n2);
  else if (my_strlen(n1) == my_strlen(n2))
    {
	if (*n1 > *n2)
	  return (n1);
	else
	  return (n2);
      }
}

char		*calc_it(char *n1, char *n2, char *res, int t)
{
  int		i;
  int		j;
  
  i = my_strlen(n1) - 1;
  j = my_strlen(n2) - 1;
  while (j != -1)
    {
      res[t] = n1[i] + (n2[j] - '0');
	t--;
	j--;
	i--;
    }
  while (t != -1 && i != -1)
    {
      res[t] = n1[i];
      i--;
      t--;
    }
  res[my_strlen(n1) + 1] = 0;
  res = clean_zero(res);
  return (adjust_res(res));
}     

char		*my_add(char *n1, char *n2, t_args *vbox)
{
  char		*res;
  int		t;
  
  res = malloc(my_strlen(my_taller(n1, n2)) + 5);
  t = my_strlen(my_taller(n1, n2)) - 1;
  return(calc_it(n1, n2, res, t));
}
