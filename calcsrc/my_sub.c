/*
** my_sub.c for bistromathique in /home/boguta_m/Projets/Bistro/calcsrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Nov  6 21:53:44 2013 maxime boguta
** Last update Sun Nov 10 18:17:58 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"
#include "defcalc.h"
#include "define.h"

char		*my_sub(char *n1, char *n2, t_args *vbox)
{
  char		*taller;
  char		*res;
  int		i;
  int		push;
  int		j;
  int		t;

  push = 1;
  taller = n2;
  i = my_strlen(n1) - 1;
  j = my_strlen(n2) - 1;
  if (i > j)
    {
      res = malloc(i + 1);
      taller = n1;
    }
  else
    res = malloc(j + 1);
  t = my_strlen(taller) - 1;
  while (j != -1)
    {
      if (n1[i] < n2[j])
	{
	  res[t] = (n1[i] + 10) - (n2[j] - '0');
	  while (n1[i - push] == '0')
	    {
	      n1[i - push] = '9';
	      push++;
	    }
	  n1[i - push]--;
	}
      else
	res[t] = n1[i] - (n2[j] - '0');
      j--;
      i--;
      t--;
    }
  while (t != -1)
    {
      res[t] = n1[i];
      t--;
      i--;
    }
  res[my_strlen(res) + 1] = '\0';
  if (my_strlen (res) != 1)
    res = clean_zero(res);
  return (res);
}
