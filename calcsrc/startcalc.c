/*
** startcalc.c for bistromathique in /home/boguta_m/Projets/Bistro/calcsrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Nov  6 20:03:49 2013 maxime boguta
** Last update Sun Nov 10 18:25:53 2013 maxime boguta
*/

#include <string.h>
#include "define.h" 
#include "prototypes.h"
#include "my.h"

int		is_taller(t_token *n1, t_token *n2)
{
  int		n1len;
  int		n2len;
  int		i;

  n1len = my_strlen(n1->val);
  n2len = my_strlen(n2->val);
  i = 0;
  if (n2len > n1len)
    return (1);
  else if (n1len < n1len)
    return (0);
  else if (n1len == n2len)
    while (i != n1len)
      {
	if (n2->val[i] > n1->val[i])
	  return (1);
	else
	  i++;
      }
  return (0);
}

char		*def_operation(char *op, t_token *n1, t_token *n2, t_args *vbox)
{
  char		*(*funcs[5])(char *n1, char *n2, t_args *vbox);
  char		i;
 
  funcs[0] = &my_add;
  funcs[1] = &my_sub;
  funcs[2] = &my_mul;
  funcs[3] = &my_div;
  funcs[4] = &my_mod;
  i = 0;
  while (vbox->op[i] != *op)
    i = i + 1;
  i = i - 2;
  if (XOR(SIGN_IS_L(n1), SIGN_IS_L(n2)) && i == 0)
    {
      if (SIGN_IS_L(n1))
	return (funcs[i + 1](n2->val, n1->val, vbox));
      else (funcs[i + 1](n1->val, n2->val, vbox));
    }
  if ((is_taller(n1, n2) == 1) && i == 0)
    return (funcs[i] (n2->val, n1->val, vbox));
  if ((SIGN_IS_L(n1) && SIGN_IS_L(n2)) && i == 1)
    return (funcs[i - 1](n1->val, n2->val, vbox));
  if ((SIGN_IS_L(n1) && SIGN_IS_L(n2)) && (i == 2 || i == 3 || i == 4)) 
    n2->sign[0] = '+';
  if (i == 1)
    {
      if (SIGN_IS_L(n1) && (is_taller(n1, n2) == 1))
	{
	  n2->sign[0] = '-';
	  return (funcs[i - 1](n2->val, n1->val, vbox));
	}
      else if (SIGN_IS_L(n2) && (is_taller(n1, n2) == 1))
	return (funcs[i](n2->val, n1->val, vbox));
      if (is_taller(n1, n2) == 0)
	{
	  if (SIGN_IS_L(n1))
	    {
	      n2->sign[0] = '-';
	      return (funcs[i - 1](n1->val, n2->val, vbox));
	    }
	  else if (SIGN_IS_L(n2))
	    return (funcs[i](n1->val, n2->val, vbox));
	}
      else
	{
	  n2->sign[0] = '-';
	  return (funcs[i](n1->val, n2->val, vbox));
	}
    }
  return (funcs[i](n1->val, n2->val, vbox));
}

char		*start_calc(t_token *list, t_args *vbox)
{
  char		*re;

  while (list->next != NULL)
    {
      while (list->type == OPERAND)
	  list = list->next;
      if (list->type != OPERAND)
	{	      
	  PREV_VAL = my_strdup(def_operation(CUR_VAL, ANTE_T, PREV_T, vbox));
	  list = list->prev;
	  if (list->next->next != NULL && list->next != NULL)
	    list->next->next->prev = list;
	  if (list->prev->prev != NULL && list->prev != NULL)
	    list->prev->prev->next = list;
	  list->next = list->next->next;
	  list->prev = list->prev->prev;
	}
      go_to_start(list);
    }
  return (CUR_VAL);
}
