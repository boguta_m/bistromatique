##
## Makefile for bistromathique in /home/boguta_m/Projets/Bistro
## 
## Made by maxime boguta
## Login   <boguta_m@epitech.net>
## 
## Started on  Tue Oct 29 18:09:55 2013 maxime boguta
## Last update Sun Nov 10 18:07:15 2013 maxime boguta
##

CC		= gcc

RM		= rm -f

NAME 		= calc

LDFLAGS		= -Llib -lmy

CFLAGS		= -Iinc -g3

SRCS		= parsesrc/to_token.c \
		  eval_expr.c \
		  parsesrc/store_arg.c \
		  parsesrc/cmp_opd.c \
		  parsesrc/isneg.c \
		  parsesrc/get_signs.c \
		  parsesrc/set_list_prev.c \
		  convsrc/convert_base_n_opr.c \
		  convsrc/to_NPI.c \
		  convsrc/list_movements.c \
		  calcsrc/my_add.c \
		  calcsrc/my_sub.c \
		  calcsrc/my_mul.c \
		  calcsrc/my_div.c \
		  calcsrc/my_mod.c \
		  calcsrc/startcalc.c \
		  calcsrc/more.c \
		  main.c

OBJS		= $(SRCS:.c=.o)



all : $(NAME)

$(NAME): $(OBJS)
	@ $(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

move :
	mv *.o $(OBJS)

go : $(NAME) clean

clean :
	$(RM) $(OBJS)

fclean : clean
	$(RM) $(NAME)

re : fclean all

.PHONY : all clean fclean re
