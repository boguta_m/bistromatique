/*
** my_strncpy.c for my_strncpy in /home/boguta_m/rendu/Piscine-C-Jour_06
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 16:00:05 2013 maxime boguta
** Last update Fri Oct 25 12:09:25 2013 maxime boguta
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (src[i] != '\0' && i != n && n > 0)
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}
