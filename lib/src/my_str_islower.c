/*
** my_str_islower.c for my_str_islower in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_12
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Oct  8 19:30:44 2013 maxime boguta
** Last update Tue Oct  8 19:54:22 2013 maxime boguta
*/

int	my_str_islower(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 'a' || str[i] > 'z')
	return (0);
      i = i + 1;
    }
  return (1);
}
