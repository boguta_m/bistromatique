/*
** my_revstr.c for my_revstr in /home/boguta_m/rendu/Piscine-C-Jour_06
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 16:21:57 2013 maxime boguta
** Last update Mon Oct 21 12:21:41 2013 maxime boguta
*/

char	*my_revstr(char *str)
{
  char	buffer;
  char	count1;
  char	count2;

  count1 = 0;
  count2 = my_strlen(str);
  while (count2 > count1)
    {
      buffer = str[count1];
      str[count1] = str[count2];
      str[count2] = buffer;
      count1 = count1 + 1;
      count2 = count2 - 1;
    }
  return (str);
}
