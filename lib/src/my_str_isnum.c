/*
** my_str_isnum.c for my_str_isnum in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_11
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Oct  8 19:09:12 2013 maxime boguta
** Last update Fri Oct 25 12:06:31 2013 maxime boguta
*/

int	my_str_isnum(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] > '9' || str[i] < '0')
	return (0);
      i = i + 1;
    }
  return (1);
}
