/*
** my_put_nbr.c for my_put_nbr in /home/boguta_m/rendu/Piscine-C-Jour_03
** 
** Made by Boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Oct  2 20:28:16 2013 Boguta
** Last update Fri Oct 25 12:00:28 2013 maxime boguta
*/

int	my_put_nbrbigfilter(int result, int div, int nb)
{
  if (nb != -2147483648 || div != -1)
    {
      result = nb / div;
      result = result % 10;
      result = result + '0';
      my_putchar(result);
    }
  else
    {
      my_putchar('8');
    }
}

int	my_put_nbrfunc(int result, int div, int isneg, int nb)
{
  div = -div;
  while (div >= nb && div > -1000000000)
    {
      div = div * 10;
    }
  if (div < -1 && div > -1000000000)
    {
      div = div / 10;
    }
  while (div != 0)
    {
      if (isneg >= 1)
	{
	  my_putchar('-');
	  isneg = 0;
	}
      my_put_nbrbigfilter(result, div, nb);
      div = div / 10;
    }
}

int	my_put_nbr(int nb)
{
  int	result;
  int	div;
  char	isneg;

  result = 0;
  div = 1;
  isneg = 0;
  if (nb >= 0)
    {
      nb = -nb;
    }
  else
    {
      isneg = 1;
    }
  my_put_nbrfunc(result, div, isneg, nb);
}
