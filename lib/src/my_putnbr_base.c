/*
** my_putnbr_base.c for my_putnbr_base in /home/boguta_m/rendu/Piscine-C-lib/my
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct 21 12:19:41 2013 maxime boguta
** Last update Mon Oct 21 12:20:55 2013 maxime boguta
*/

int		my_putnbr_base(int nbr, char *base)
{
  long long	div;
  long long	n;
  int		a;
  int		len;
  char		c;

  a = 0;
  div = 1;
  n = nbr;
  len = my_strlen(base);
  if ( n < 0)
    {
      my_putchar('-');
      n = -n;
    }
  while (n/div > 1)
    {
      div = div * len;
    }
  while (div > 1)
    {
      div = div / len;
      c = n / div %  len;
      my_putchar(base[c]);
    }
}
