/*
** my_putchar.c for my_putchar in /home/boguta_m/rendu/Piscine-C-lib/my
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct 21 11:05:11 2013 maxime boguta
** Last update Fri Oct 25 11:59:15 2013 maxime boguta
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
