/*
** my_strupcase.c for my_strupcase in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_07
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 22:01:42 2013 maxime boguta
** Last update Fri Oct 25 12:11:54 2013 maxime boguta
*/

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= 97 && str[i] <= 122)
	str[i] = str[i] - 32;
      i = i + 1;
    }
  return (str);
}
