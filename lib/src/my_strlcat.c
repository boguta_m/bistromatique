/*
** my_strlcat.c for my_strlcat in /home/boguta_m/rendu/Piscine-C-lib/my
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct 21 12:25:40 2013 maxime boguta
** Last update Fri Oct 25 12:07:39 2013 maxime boguta
*/

int	my_strlcat(char *dest, char *src, int size)
{
  int	i;
  int	s1_len;
  int	s2_len;

  i = 0;
  s1_len = my_strlen(dest);
  s2_len = my_strlen(src);
  if (s1_len + s2_len >= size)
    return (size + s2_len);
  while (src[i] != '\0')
    {
      dest[s1_len + i] = src[i];
      i = i + 1;
    }
  return (s1_len + s2_len);
}
