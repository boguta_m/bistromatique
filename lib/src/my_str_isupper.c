/*
** my_str_isupper.c for my_str_isupper in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_13
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Oct  8 19:53:43 2013 maxime boguta
** Last update Fri Oct 25 12:07:12 2013 maxime boguta
*/

int	my_str_isupper(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 'A' || str[i] > 'Z')
	return (0);
      i = i + 1;
    }
  return (1);
}
