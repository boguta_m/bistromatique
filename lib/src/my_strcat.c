/*
** my_strcat.c for my_strcat in /home/boguta_m/rendu/Piscine-C-Jour_07
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Oct  9 20:54:59 2013 maxime boguta
** Last update Fri Oct 25 12:03:25 2013 maxime boguta
*/

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	j;
  int	dest_len;

  j = 0;
  i = 0;
  while (dest[i] != '\0')
    {
      i = i + 1;
    }
  while (src[j] != '\0')
    {
      dest[i] = src[j];
      j = j + 1;
      i = i + 1;
    }
  dest[i + j] = 0;
  return (dest);
}
