/*
** my_strlowcase.c for my_strlowcase in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_08
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 22:01:19 2013 maxime boguta
** Last update Fri Oct 25 12:08:37 2013 maxime boguta
*/

char	*my_strlowcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= (97 - 32) && str[i] <= (122 - 32))
	str[i] = str[i] + 32;
      i = i + 1;
    }
  return (str);
}
