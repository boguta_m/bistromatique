/*
** my_swap.c for my_swap in /home/boguta_m/rendu/Piscine-C-Jour_04
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Fri Oct  4 11:53:53 2013 maxime boguta
** Last update Fri Oct 25 12:12:07 2013 maxime boguta
*/

int	my_swap(int *a, int *b)
{
  int	buffer;

  buffer = 0;
  buffer = *a;
  *a = *b;
  *b = buffer;
}
