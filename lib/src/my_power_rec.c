/*
** my_power_rec.c for my_power_rec in /home/boguta_m/rendu/Piscine-C-Jour_05
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 12:00:24 2013 maxime boguta
** Last update Fri Oct 25 11:58:09 2013 maxime boguta
*/

int	my_power_rec(int nb, int power)
{
  int	result;

  result = nb;
  if (power == 0)
    return (1);
  while (power > 1)
    {
      result = result * nb;
      power = power - 1;
      my_power_rec(nb, power);
    }
  return (result);
}
