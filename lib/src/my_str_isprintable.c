/*
** my_str_isprintable.c for my_str_isprintable in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_14
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Oct  8 19:55:27 2013 maxime boguta
** Last update Fri Oct 25 12:06:49 2013 maxime boguta
*/

int	my_str_isprintable(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] <= 31 || str[i] == 127)
	return (0);
      i = i + 1;
    }
  return (1);
}
