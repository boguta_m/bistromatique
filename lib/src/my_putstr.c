/*
** my_putstr.c for my_putstr in /home/boguta_m/rendu/Piscine-C-Jour_04
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Fri Oct  4 12:27:24 2013 maxime boguta
** Last update Tue Nov  5 17:45:38 2013 maxime boguta
*/

int	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}
