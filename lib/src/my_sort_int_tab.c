/*
** my_sort_int_tab.c for my_sort_int_tab in /home/boguta_m/rendu/Piscine-C-lib/my
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct 21 12:22:20 2013 maxime boguta
** Last update Wed Oct 23 19:28:42 2013 maxime boguta
*/

void my_sort_int_tab(int *tab, int size)
{
  int	i;
  int	j;
  int	tmp;

  i = 0;
  j = 1;
  while (j != 0)
    {
      i = 0;
      j = 0;
      while (i < size - 1)
	{
	  if (tab[i] > tab[i + 1])
	    {
	      tmp = tab[i];
	      tab[i] = tab[i + 1];
	      tab[i + 1] = tmp;
	      j = j + 1;
	    }
	  i = i + 1;
	}
    }
}
