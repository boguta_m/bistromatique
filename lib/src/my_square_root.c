/*
** my_square_root.c for my_square_root in /home/boguta_m/rendu/Piscine-C-lib/my
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct 21 14:07:48 2013 maxime boguta
** Last update Fri Oct 25 12:02:10 2013 maxime boguta
*/

int	my_square_root(int nb)
{
  int	i;

  i = 0;
  if (nb == 1)
    {
      return (1);
    }
  while (i <= (nb / 2))
    {
      if ((i * i) == nb)
	{
	  return (i);
	}
      i = i + 1;
    }
  return (0);
}
