/*
** my_strstr.c for my_strstr in /home/boguta_m/rendu/Piscine-C-Jour_06
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 17:47:57 2013 maxime boguta
** Last update Fri Oct 25 12:17:50 2013 maxime boguta
*/

#include <string.h>
#include "my.h"

char	*my_strstr(char *str, char* to_find)
{
  int	str_count;
  int	to_find_count;

  str_count = 0;
  to_find_count = 0;
  while (str_count < my_strlen(str) - 1)
    {
      while (str[str_count] == to_find[to_find_count])
	{
	  to_find_count = to_find_count + 1;
	  str_count = str_count + 1;
	  if (to_find_count  == my_strlen(to_find))
	    {
	      return (str + (str_count - to_find_count));
	    }
	}
      to_find_count = 0;
      str_count = str_count + 1;
    }
  return (NULL);
}
