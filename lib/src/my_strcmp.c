/*
** my_strcmp.c for my_strncmp in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_05
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 22:02:38 2013 maxime boguta
** Last update Sun Oct 27 05:47:49 2013 maxime boguta
*/

int     my_strcmp(char *s1, char *s2)
{
  int   i;

  i = 0;
  while (s1[i])
    {
      if (s1[i] < s2[i])
	return (-1);
      if (s1[i] > s2[i])
	return (1);
      i = i + 1;
    }
  if (s1[i] < s2[i])
    return (-1);
  if (s1[i] > s2[i])
    return (1);
  return (0);
}
