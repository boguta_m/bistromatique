/*
** my_strlen.c for my_strlen in /home/boguta_m/rendu/Piscine-C-Jour_04
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Fri Oct  4 12:42:26 2013 maxime boguta
** Last update Tue Nov  5 18:25:33 2013 maxime boguta
*/

int	my_strlen(char *str)
{
  int	strlen;

  strlen = 0;
  while (*str != '\0')
    {
      strlen = strlen + 1;
      str = str + 1;
    }
  return (strlen);
}
