/*
** my_strcpy.c for my_strcpy in /home/boguta_m/rendu/Piscine-C-Jour_06
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 13:55:56 2013 maxime boguta
** Last update Mon Nov  4 17:33:23 2013 maxime boguta
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = 0;
  return (dest);
}
