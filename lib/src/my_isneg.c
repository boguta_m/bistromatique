/*
** my_isneg.c for my_isneg in /home/boguta_m/rendu/Piscine-C-Jour_03
** 
** Made by Boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Oct  2 12:07:33 2013 Boguta
** Last update Fri Oct 25 11:57:30 2013 maxime boguta
*/

int	my_isneg(int n)
{
  if (n >= 0)
    {
      my_putchar('P');
    }
  else
    {
      my_putchar('N');
    }
}
