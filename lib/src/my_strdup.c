/*
** my_strdup.c for my_strdup in /home/boguta_m/rendu/Piscine-C-Jour_08/ex_01
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Oct 10 10:10:13 2013 maxime boguta
** Last update Thu Oct 31 15:42:41 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"

char	*my_strdup(char *src)
{
  int	l;
  char	*strdupped;

  l = my_strlen(src);
  strdupped = malloc(l);
  strdupped = my_strcpy(strdupped, src);
  if (strdupped != 0)
    return (strdupped);
  else
    return (0);
}
