/*
** bistromathique.h for bistromathique in /home/boguta_m/Projets/Bistro
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Oct 29 18:03:28 2013 maxime boguta
** Last update Sun Nov 10 23:21:32 2013 maxime boguta
*/

#ifndef BISTROMATHIQUE_H_
#define BISTRO_MATHIQUE_H

#define OP_OPEN_PARENT_IDX 0
#define OP_CLOSE_PARENT_IDX 1
#define OP_PLUS_IDX 2
#define OP_SUB_IDX 3
#define OP_NEG_IDX 3
#define OP_MULT_IDX 4
#define OP_DIV_IDX 5
#define OP_MOD_IDX 6

#define SYNTAXE_ERROR_MSG "syntax error\n"

char *eval_expr(char *base,char *ops,char *expr,unsigned int size);

#endif
