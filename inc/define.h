/*
** header.h for eval_expr in /home/boguta_m/rendu/Piscine-C-eval_expr
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Fri Oct 25 12:21:39 2013 maxime boguta
** Last update Sun Nov 10 18:06:53 2013 maxime boguta
*/

#ifndef HEADER_H_
#define HEADER_H_

#define p(x) my_putchar((x))
#define OP_PAR 0
#define CL_PAR 1
#define OPERATOR 3
#define PRI_OPERATOR 2
#define OPERAND 4
#define IS_CHAR_OPR(x) (x) == vbox->op[4] || (x) == vbox->op[5] || (x) == vbox->op[6] || (x) == vbox->op[2] || (x) == vbox->op[3]
#define IS_CHAR_OPD(x) is_operand((x), vbox)
#define IS_CHAR_OPR_PRI(x) (x) == vbox->op[4] || (x) == vbox->op[5] || (x) == vbox->op[6]
#define IS_CHAR_SIGN(x) (x) == vbox->op[2] || (x) == vbox->op[3]
#define CUR_TYPE list->type
#define PIL_TYPE pile->type
#define CP_TYPES(x) CUR_TYPE == (x) && PIL_TYPE == (x)
#define CP_BOTH (CP_TYPES(OPERATOR) || CP_TYPES(PRI_OPERATOR))
#define CP_SWAP (CUR_TYPE == OPERATOR && PIL_TYPE == PRI_OPERATOR)
#define SWITCH_OUT pile != NULL && (CP_BOTH || CP_SWAP)
#define CUR_VAL list->val
#define PREV_VAL (list->prev)->val
#define ANTE_VAL ((list->prev)->prev)->val
#define ANTE_T (list->prev)->prev
#define PREV_T (list->prev)
#define PUSHOUT push - my_strlen(n2)
#define SIGN_IS_L(x) (((x)->sign[0] == '-' || (x)->sign[0] == vbox->op[3]) && (x)->sign[0] != 0)
#define XOR(a, b) ((a || b) && !(a && b))

struct s_token
{
  char			type;
  char			*val;
  char			sign[1];
  struct s_token	*next;
  struct s_token	*prev;
};
typedef struct s_token t_token;

struct s_args
{
  int			base_len;
  char			op[7];
  char			*base;
};
typedef struct s_args t_args;

#endif
