/*
** defcalc.c for bistromathique in /home/boguta_m/Projets/Bistro/inc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov  7 22:57:52 2013 maxime boguta
** Last update Sun Nov 10 18:19:01 2013 maxime boguta
*/

#ifndef DEFCALC_H_
#define DEFCALC_H

char	*clean_zero(char *);

#endif
