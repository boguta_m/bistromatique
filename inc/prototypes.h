/*
** prototypes.h for bistromathique in /home/boguta_m/Projets/Bistro/inc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Oct 31 16:29:17 2013 maxime boguta
** Last update Sun Nov 10 18:15:48 2013 maxime boguta
*/

#ifndef PROTOTYPES_H_
#define PROTOTYPES_H_

t_token		*convert_base_n_op(t_token *, t_args *);
t_token		*attrib_to_token(char *, t_args *);
t_args		*store_arguments(t_args *, char *, char *);
char		*which_sign(char *, t_args *);
t_token		*get_signs(t_token *, t_args *);
t_token		*go_to_end(t_token *);
t_token		*go_to_start(t_token *);
t_token		*go_to_last_bracket(t_token *);
t_token		*to_npi(t_token *, t_args *);
char		*start_calc(t_token *, t_args *);
char		*my_add(char *, char *, t_args *);
char		*my_sub(char *, char *, t_args *);
char		*my_mul(char *, char *, t_args *);
char		*my_div(char *, char *, t_args *);
char		*my_mod(char *, char *, t_args *);

#endif
