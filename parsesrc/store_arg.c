/*
** store_arg.c for bistromathique in /home/boguta_m/Projets/Bistro
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Oct 29 21:33:41 2013 maxime boguta
** Last update Thu Oct 31 16:39:57 2013 maxime boguta
*/

#include <stdlib.h>
#include "define.h"
#include "my.h"

t_args		*store_arguments(t_args *vbox, char *base, char *op)
{
  char		i;
  
  i = 0;
  vbox = malloc(sizeof(t_args));
  vbox->base_len = my_strlen(base);
  vbox->base = my_strdup(base);
  while (op[i])
    {
      vbox->op[i] = op[i];
      i = i + 1;
    }
  return (vbox);
}
