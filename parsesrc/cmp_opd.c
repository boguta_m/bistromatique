/*
** cmp_opd.c for bistromathique in /home/boguta_m/Projets/Bistro/parsesrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Oct 31 14:37:01 2013 maxime boguta
** Last update Mon Nov  4 15:59:57 2013 maxime boguta
*/

#include "define.h"

int		is_operand(char in, t_args *vbox)
{
  int		i;

  i = 0;
  while (vbox->base[i] != 0)
    {
      if (in == vbox->base[i])
	return (1);
      i = i + 1;
    }
  return (0);
}
