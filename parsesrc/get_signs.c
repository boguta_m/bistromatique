/*
** get_signs.c for bistromathique in /home/boguta_m/Projets/Bistro/parsesrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Nov  4 20:29:22 2013 maxime boguta
** Last update Sat Nov  9 18:58:26 2013 maxime boguta
*/

#include <string.h>
#include "define.h"

char		check_par(t_token *list, t_args *vbox)
{
  unsigned int	op_par_amt;
  unsigned int	cl_par_amt;

  op_par_amt = 0;
  cl_par_amt = 0;
  while (list != NULL)
    {
      if (list->type == OP_PAR)
	op_par_amt = op_par_amt + 1;
      if (list->type == CL_PAR)
	cl_par_amt = cl_par_amt + 1;
      list = list->next;
    }
  if (cl_par_amt == op_par_amt)
    return (0);
  else
    return (1);
}

char		check_for_syntax(t_token *list, t_args *vbox)
{
  unsigned int	op_cnt;
  unsigned int	opr_cnt;
  t_token	*save;

  save = list;
  op_cnt = 0;
  opr_cnt = 0;
  while (list != NULL)
    {
      if (list->type == OPERAND)
	op_cnt = op_cnt + 1;
      if (IS_CHAR_OPR(list->val[0]))
	opr_cnt = opr_cnt + 1;
      list = list->next;
    }
  if (opr_cnt == 0 || (op_cnt != (opr_cnt + 1)))
    return (1);
  else
    return (check_par(save, vbox));
}
  

t_token		*get_first(t_token *list, t_args *vbox)
{
  t_token	*save;

  save = list;
  while (list != NULL)
    {
      if (list->type == 4)
	list->sign[0] = '+';
      list = list->next;
    }
  list = save;
  if (IS_CHAR_SIGN(*(list->val)) && (list->next)->type == OPERAND)
    {
      list->next->sign[0] = list->val[0];
      list = list->next;
    }
  return (list);
}

t_token		*get_signs(t_token *list, t_args *vbox)
{
  t_token	*save;

  list = get_first(list, vbox);
  save = list;
  while (list != NULL)
    {
      if (list->prev != NULL && list->type == 4)
	if ((list->prev)->type == OPERATOR)
	  {
	    if ((list->prev)->prev != NULL && ((list->prev)->prev)->type != 4)
	      if (((list->prev)->prev)->type != CL_PAR)
		{
		  list->sign[0] = (list->prev)->val[0];
		  ((list->prev)->prev)->next = list;
		}
	  }
      list = list->next;
    }
  if (check_for_syntax(save, vbox) == 1)
    return (NULL);
  return (save);
}
