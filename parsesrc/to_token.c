/*
** to_token.c for eval_expr in /home/boguta_m/rendu/Piscine-C-eval_expr
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Fri Oct 25 14:51:53 2013 maxime boguta
** Last update Tue Nov  5 15:55:50 2013 maxime boguta
*/

#include <stdlib.h>
#include <string.h>
#include "define.h"
#include "prototypes.h"

void		set_nodes_type(t_token *elem, char *in, t_args *vbox)
{
  if (*in == vbox->op[0])
    elem->type = OP_PAR;
  if (*in == vbox->op[1])
    elem->type = CL_PAR;
  if (IS_CHAR_OPR(*in))
    elem->type = OPERATOR;
  if (IS_CHAR_OPR_PRI(*in))
    elem->type = PRI_OPERATOR;
  if (IS_CHAR_OPD(*in))
    elem->type = OPERAND;
  else if (IS_CHAR_OPR(*in) || *in == vbox->op[0] || *in == vbox->op[1])
    elem->val[0] = *in;
  else
    in = in + 1;
}

unsigned int	my_set_nodes(t_token *elem, char *in, t_args *vbox)
{
  unsigned int	i;
  unsigned int	j;

  i = 0;
  j = 0;
  while (*in == vbox->base[0] && IS_CHAR_OPD(*(in + 1)))
    {
      in = in + 1;
      j = j + 1;
    }
  elem->val = malloc(count_stack(in + 1, vbox));
  set_nodes_type(elem, in, vbox);
  while (IS_CHAR_OPD(*in))
    {
      elem->val[i] = *in;
      in = in + 1;
      i = i + 1;
    }
  elem->val[count_stack(in - i, vbox)] = 0;
  return (i + j);
}

int		count_stack(char *in, t_args *vbox)
{
  unsigned int	i;

  i = 0;
  if (IS_CHAR_OPD(in[i]))
    {
      while (IS_CHAR_OPD(in[i]))
	i = i + 1;
      return (i);
    }
  else
    return (1);
}

int	 	my_put_in_list(t_token **list, char *in, t_args *vbox)
{
  t_token	*elem;
  unsigned int	i;
  t_token	*save;

  save = *list;
  elem = malloc(sizeof(*elem));
  elem->next = NULL;
  i = my_set_nodes(elem, in, vbox);
  if (*list == NULL)
    {
      *list = elem;
      if (i == 0)
	return (1);
      return (i);
    }
  while ((*list)->next != NULL)
    *list = (*list)->next;
  (*list)->next = elem;
  *list = save;
  if (i == 0)
    return (1);
  return (i);
}

t_token		*attrib_to_token(char *in, t_args *vbox)
{
  unsigned int	i;
  t_token	*list;

  list = NULL;
  while (*in != 0)
    {
      i = my_put_in_list(&list, in, vbox);
      in = in + i;
    }
  set_prev(list);
  if (convert_base_n_op(list, vbox) == NULL)
    return (NULL);
  return (list);
}
