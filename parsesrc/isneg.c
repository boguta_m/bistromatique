/*
** isneg.c for bistromathique in /home/boguta_m/Projets/Bistro/parsesrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Nov  4 16:02:02 2013 maxime boguta
** Last update Tue Nov  5 21:24:39 2013 maxime boguta
*/

#include "define.h"
#include "my.h"
#include "prototypes.h"
#include "bistromathique.h"

void		clean_ops(char *str, t_args *vbox)
{
  while (*str)
    {
      if (*str == vbox->op[2] && IS_CHAR_OPD(*(str + 1) == 0))
	if (*(str + 1) != OP_PAR && *(str + 1) != CL_PAR)
	  *str = ' ';
      if (*str == vbox->op[2] && *(str + 1) == ' ')
	*str = ' ';
      str = str + 1;
    }
}

char		*gimme_sign(char *str, t_args *vbox)
{
  int		amt;

  amt = 0;
  while (IS_CHAR_SIGN(*str))
    {
      if (*str == vbox->op[3])
	{
	  *str = ' ';
	  amt = amt + 1;
	}
      str = str + 1;
    }
  if (amt % 2 == 1)
    *(str - 1) = vbox->op[3];
  else
    *(str - 1) = vbox->op[2];
  return (str);
}

char		*which_sign(char *strin, t_args *vbox)
{
  char		*str;

  str = my_strdup(strin);
  strin = str;
  while (*str)
    {
      if (IS_CHAR_SIGN(*str))
	str = gimme_sign(str, vbox);
      str = str + 1;
    }
  clean_ops(strin, vbox);
  return (strin);
}
