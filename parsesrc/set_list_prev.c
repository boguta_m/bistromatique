/*
** set_list_prev.c for bistromathique in /home/boguta_m/Projets/Bistro/parsesrc
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Nov  5 15:51:49 2013 maxime boguta
** Last update Tue Nov  5 16:04:55 2013 maxime boguta
*/

#include <string.h>
#include "define.h"

void		set_prev(t_token *list)
{
  t_token	*current;

  list->prev = NULL;
  while (list != NULL)
    {
      current = list;
      list = list->next;
      if (list != NULL)
	list->prev = current;
    }
}
